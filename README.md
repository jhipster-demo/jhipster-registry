### JHipster Registry

* [Overview](https://www.jhipster.tech/jhipster-registry/)
* [WAR releases](https://github.com/jhipster/jhipster-registry/releases)
* [Github Project](https://github.com/jhipster/jhipster-registry/)

### Running

1. Download [WAR file](https://github.com/jhipster/jhipster-registry/releases) (tested with v4.0.6)
1. Run ``./run-register.bat``